"""
Unit tests for api.py
"""
import datetime
from types import SimpleNamespace
from uuid import UUID
from fastapi.testclient import TestClient
from ..api import app

all_inventories = [
    [
        "Hundmat",
        27,
        "Den Lilla Djurbutiken"
    ],
    [
        "Kattmat",
        170,
        "Den Lilla Djurbutiken"
    ],
    [
        "Hundmat",
        140,
        "Den Stora Djurbutiken"
    ],
    [
        "Kattklonare",
        68,
        "Den Stora Djurbutiken"
    ],
    [
        "Kattmat",
        345,
        "Den Stora Djurbutiken"
    ],
    [
        "Sömnpiller och energidryck för djur",
        61,
        "Den Stora Djurbutiken"
    ],
    [
        "Elefantkoppel",
        27,
        "Djuristen"
    ],
]

return_data = [
  {
    "product_name": "Hundmat",
    "adjusted_quantity": 27,
    "store_name": "Den Lilla Djurbutiken"
  },
  {
    "product_name": "Kattmat",
    "adjusted_quantity": 170,
    "store_name": "Den Lilla Djurbutiken"
  },
  {
    "product_name": "Hundmat",
    "adjusted_quantity": 140,
    "store_name": "Den Stora Djurbutiken"
  },
  {
    "product_name": "Kattklonare",
    "adjusted_quantity": 68,
    "store_name": "Den Stora Djurbutiken"
  },
  {
    "product_name": "Kattmat",
    "adjusted_quantity": 345,
    "store_name": "Den Stora Djurbutiken"
  },
  {
    "product_name": "Sömnpiller och energidryck för djur",
    "adjusted_quantity": 61,
    "store_name": "Den Stora Djurbutiken"
  },
  {
    "product_name": "Elefantkoppel",
    "adjusted_quantity": 27,
    "store_name": "Djuristen"
  }
]

def db_mock(data):
    """This function returns a database mocking object, that will be used
    instead of the actual db connection.
    """
    database = SimpleNamespace()
    database.cursor = CursorMock(data)
    return database


class CursorMock:
    """This class mocks a db cursor. It does not build upon unittest.mock but
    it is instead built from an empty class, patching manually all needed
    methods.
    """
    def __init__(self, data):
        self.data = data

    def __enter__(self):
        return self

    def __exit__(self, *args):
        pass

    def __call__(self):
        return self

    @staticmethod
    def execute(*args):
        """This mocks cursor.execute. It returns args even though the return
        value of cursor.execute() is never used. This is to avoid the
        following linting error:

        W0613: Unused argument 'args' (unused-argument)
        """
        return args

    def fetchall(self):
        """This mocks cursor.fetchall.
        """
        return self.data


def test_stores():
    """
    Testing the /stores endpoint
    """
    app.db = db_mock([
        ['Djurjouren', 'Stockholm', 'Upplandsgatan 99', '12345'],
        ['Djuristen', 'Falun', 'Skånegatan 420', '54321'],
        ['Den Lilla Djurbutiken', 'Hudiksvall', 'Nätverksgatan 22', '55555'],
        ['Den Stora Djurbutiken', 'Hudiksvall', 'Routergatan 443', '54545'],
        ['Noahs Djur & Båtaffär', 'Gävle', 'Stallmansgatan 666', '96427']
    ])


    client = TestClient(app)
    response = client.get("/stores")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "name": "Djurjouren",
                "address": "Upplandsgatan 99, 12345 Stockholm"
            },
            {
                "name": "Djuristen",
                "address": "Skånegatan 420, 54321 Falun"
            },
            {
                "name": "Den Lilla Djurbutiken",
                "address": "Nätverksgatan 22, 55555 Hudiksvall"
            },
            {
                "name": "Den Stora Djurbutiken",
                "address": "Routergatan 443, 54545 Hudiksvall"
            },
            {
                "name": "Noahs Djur & Båtaffär",
                "address": "Stallmansgatan 666, 96427 Gävle"
            }
        ]
    }


def test_stores_name():
    """
    Testing the /stores/{name} endpoint
    """
    app.db = db_mock([('Djuristen', 'Falun', 'Skånegatan 420', '54321')])
    client = TestClient(app)
    response = client.get("/stores/Djuristen")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "name": "Djuristen",
                "address": "Skånegatan 420, 54321 Falun"
            }
        ]
    }



def test_stores_name_failed():
    """
    Testing the /stores/{name} endpoint
    """
    app.db = db_mock([])
    client = TestClient(app)
    response = client.get("/stores/nonexistingstore")
    assert response.status_code == 404
    assert response.json() == {"detail": "404 Not Found"}



def test_cities():
    """
    Testing the /cities endpoint
    """
    app.db = db_mock([('Falun',), ('Gävle',), ('Stockholm',), ('Hudiksvall',)])
    client = TestClient(app)
    response = client.get("/cities")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            "Falun",
            "Gävle",
            "Stockholm",
            "Hudiksvall"
        ]
    }



def test_cities_zip():
    """
    Testing the /cities endpoint
    """
    app.db = db_mock([('Stockholm',)])
    client = TestClient(app)
    response = client.get("/cities?zipcode=12345")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            "Stockholm"
        ]
    }



def test_cities_failed():
    """
    Testing the /cities endpoint
    """
    app.db = db_mock([])
    client = TestClient(app)
    response = client.get("/cities?zipcode=99999")
    assert response.status_code == 404
    assert response.json() == {"detail": "404 Not Found"}



def test_sales():
    """
    Testing the /sales endpoint
    """
    app.db = db_mock([
        ('Den Stora Djurbutiken',
         datetime.datetime(2022, 1, 25, 13, 52, 34),
         UUID('0188146f-5360-408b-a7c5-3414077ceb59')),
        ('Djuristen',
         datetime.datetime(2022, 1, 26, 15, 24, 45),
         UUID('726ac398-209d-49df-ab6a-682b7af8abfb')),
        ('Den Lilla Djurbutiken',
         datetime.datetime(2022, 2, 7, 9, 0, 56),
         UUID('602fbf9d-2b4a-4de2-b108-3be3afa372ae')),
        ('Den Stora Djurbutiken',
         datetime.datetime(2022, 2, 27, 12, 32, 46),
         UUID('51071ca1-0179-4e67-8258-89e34b205a1e'))])
    client = TestClient(app)
    response = client.get("/sales")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "store": "Den Stora Djurbutiken",
                "timestamp": "20220125T13:52:34",
                "sale_id": "0188146f-5360-408b-a7c5-3414077ceb59"
            },
            {
                "store": "Djuristen",
                "timestamp": "20220126T15:24:45",
                "sale_id": "726ac398-209d-49df-ab6a-682b7af8abfb"
            },
            {
                "store": "Den Lilla Djurbutiken",
                "timestamp": "20220207T09:00:56",
                "sale_id": "602fbf9d-2b4a-4de2-b108-3be3afa372ae"
            },
            {
                "store": "Den Stora Djurbutiken",
                "timestamp": "20220227T12:32:46",
                "sale_id": "51071ca1-0179-4e67-8258-89e34b205a1e"
            }
        ]
    }



def test_sales_id():
    """
    Testing /sales/{sale_id} endpoint when provided with working UUID
    """
    app.db = db_mock([
        (
            'Den Stora Djurbutiken',
            datetime.datetime(2022, 1, 25, 13, 52, 34),
            UUID('0188146f-5360-408b-a7c5-3414077ceb59'),
            'Hundmat',
            3
        ),
        (
            'Den Stora Djurbutiken',
            datetime.datetime(2022, 1, 25, 13, 52, 34),
            UUID('0188146f-5360-408b-a7c5-3414077ceb59'),
            'Sömnpiller och energidryck för djur',
            12
        )
    ])
    client = TestClient(app)
    response = client.get("/sales/0188146f-5360-408b-a7c5-3414077ceb59")
    assert response.status_code == 200
    assert response.json() == {
        "data": {
            "store": "Den Stora Djurbutiken",
            "timestamp": "20220125T13:52:34",
            "sale_id": "0188146f-5360-408b-a7c5-3414077ceb59",
            "products": [
                {
                    "name": "Hundmat",
                    "quantity": 3
                },
                {
                    "name": "Sömnpiller och energidryck för djur",
                    "quantity": 12
                }
            ]
        }
    }



def test_sales_id_missing():
    """
    Testing /sales/{sale_id} endpoint when provided with a non existing UUID
    """
    app.db = db_mock([])
    client = TestClient(app)
    response = client.get("/sales/19e67404-6e35-45b7-8d6f-e5bc5b79c453")
    assert response.status_code == 404
    assert response.json() == {"detail": "404 Not Found"}



def test_sales_id_not_valid():
    """
    Testing /sales/{sale_id} endpoint when provided with an invalid UUID
    """
    app.db = db_mock("invalid-uuid-format")
    client = TestClient(app)
    response = client.get("/sales/invalid-uuid-format")
    assert response.status_code == 422
    assert response.json() == {
        "detail": 'Invalid UUID given for sale_id!'}
