"""
This is an api
"""
from typing import Optional
from uuid import UUID

from fastapi import FastAPI, HTTPException
from fastapi.responses import JSONResponse

import psycopg

app = FastAPI()


@app.on_event("startup")
def startup():
    """
    This creates a connection to the database
    """
    app.db = psycopg.connect("postgresql://postgres:testpass@db/postgres")


@app.on_event("shutdown")
def shutdown():
    """
    This closes the connection
    """
    app.db.close()


@app.get("/stores")
def stores():
    """
    Endpoint for stores
    """
    with app.db.cursor() as cur:
        cur.execute("""SELECT stores.name,
        store_addresses.city,
        store_addresses.address,
        store_addresses.zip FROM stores JOIN
        store_addresses ON stores.id=store_addresses.store;""")
        db_response = cur.fetchall()
        data = []
        for store in db_response:
            name, city, street, zip_code = store
            address = f"{street}, {zip_code} {city}"
            data.append({"name": name, "address": address})
        return JSONResponse(content={"data": data})


@app.get("/stores/{name}")
def stores_name(name: str):
    """
    Endpoint for stores with more steps
    """
    with app.db.cursor() as cur:
        cur.execute("""SELECT stores.name,
        store_addresses.city,
        store_addresses.address,
        store_addresses.zip FROM stores JOIN store_addresses ON
        stores.id=store_addresses.store WHERE name = %s;""", (name,))
        db_response = cur.fetchall()
        if not db_response:
            raise HTTPException(status_code=404, detail="404 Not Found")
        data = []
        for store in db_response:
            name, city, street, zip_code = store
            address = f"{street}, {zip_code} {city}"
            data.append({"name": name, "address": address})
        return JSONResponse(content={"data": data})


@app.get("/cities")
def cities(zipcode: Optional[str] = None):
    """
    Endpoint for cities
    """
    with app.db.cursor() as cur:
        if zipcode:
            cur.execute("""
            SELECT city
            FROM store_addresses
            WHERE zip=%s;
            """, (zipcode,))
            db_response = cur.fetchall()
            if not db_response:
                raise HTTPException(status_code=404, detail="404 Not Found")
            return JSONResponse(content={"data": db_response[0]})

        cur.execute("SELECT distinct city FROM store_addresses")
        db_response = cur.fetchall()
        return JSONResponse(content={"data": [city_list[0] for city_list in db_response]})


@app.get("/sales")
def sales():
    """
    Endpoint for returning sales
    """
    with app.db.cursor() as cur:
        cur.execute("""
        SELECT stores.name, sales.time, sales.id
        FROM sales JOIN stores ON sales.store=stores.id
        """)
        db_response = cur.fetchall()
        data = []
        for sale in db_response:
            name, date_time, sale_id = sale
            data.append({"store": name,
                         "timestamp": str(date_time).replace("-", "").replace(" ", "T"),
                         "sale_id": str(sale_id)})

        return JSONResponse(content={"data": data})


@app.get("/sales/{sale_id}")
def sales_from_id(sale_id):
    """
    Endpoint for returning sales upon given a valid sale_id
    """
    try:
        UUID(sale_id)
    except ValueError as err:
        raise HTTPException(status_code=422,
                            detail="Invalid UUID given for sale_id!") from err
    with app.db.cursor() as cur:
        cur.execute("""
        SELECT stores.name, sales.time, sales.id, products.name, sold_products.quantity
        FROM stores
        JOIN sales ON sales.store=stores.id
        JOIN sold_products ON sold_products.sale=sales.id
        JOIN products ON products.id=sold_products.product
        WHERE sales.id=%s;
        """, (sale_id,))
        db_response = cur.fetchall()
        if not db_response:
            raise HTTPException(status_code=404, detail="404 Not Found")
        name, date_time, sale_id = db_response[0][:3]
        data = []
        for products in db_response:
            product_name = products[-2]
            product_quantity = products[-1]
            data.append({"name": product_name, "quantity": product_quantity})
        return JSONResponse(content={
            "data": {"store": name,
                     "timestamp": str(date_time).replace("-", "").replace(" ", "T"),
                     "sale_id": str(sale_id),
                     "products": data}})
